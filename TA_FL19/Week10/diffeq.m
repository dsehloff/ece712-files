R = 50;
C = 10*1e-12;
points = 1e3;
t_r = 30*1e-19; %(s)
f_base = 200; %(Hz)
T = 1/f_base; %(s)
V0 = 350;

t0 = 0.1*3*t_r;

t = linspace(0,3*t_r,points);
f1 = (1/t_r) * (1-exp(-(t-t0)/(R*C)));
f2 = ((t_r-1)/t_r) * (1-exp(-(t-t0-t_r)/(R*C)));
u1 = t >= t0;
u2 = t >= t0+t_r;

vc = V0*u1.*f1 + V0*u2.*f2;
plot(t,vc)

vr = V0*u1 + vc;

figure
plot(t,vr)

w = pi/t_r;
vr = V0*w*R*C/(w*R*C+1)