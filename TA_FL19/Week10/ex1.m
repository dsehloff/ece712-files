clear all
close all
clc
set(0,'DefaultFigureWindowStyle', 'normal')
% Bode plots
s = tf('s');
wMin = 10;
wMax = 1*10^12;
w = logspace(log10(wMin),log10(wMax),5000)';
%specify range
Vin = 350; % volts
tr  = 21*10^-9;    % sec
F   = 200;  % hz
w1  = F*2*pi(); % rad/s
R   = 50;   % ohms
Cp  = 10*10^-12;    % ferrads
x1  =  2 / ((tr));
x2  =  1 / (R*Cp);
G1 =    (Vin);
G2 =    ( 1 / ( 1 + (s/w1) ) );
G3 =    ( 1 / ( 1 + (s/x1) ) );
G4 =    ( 1 / ( 1 + (x2/s) ) );
sysG = G1* G2 * G3* G4;

n = 1:6000;
t = linspace(0,2/F,1e4);
u = sum(sin(n'*2*pi*F*t));
plot(t,u)
figure
lsim(sysG, u, t)