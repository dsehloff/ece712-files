function matrix()
v_in = 1;

T = 2;
t = linspace(0, T, 1e3);
phi = 0;
m = 1;
m_t = m * cos(2*pi*t - phi);
v = v_in/(n-1) * ((-n+1):2:(n-1));
i_set = -N:N;
figure
subplot(length(i_set)-1, 1, 1)
for ii = 1:(length(i_set)-1)
    i = i_set(ii);
    w = membership(i, v, m_t, N);
    subplot(length(i_set)-1, 1, ii)
    hold on
    plot(t, w, t, m_t, 'LineWidth',2)
    for j = 1:length(v)
        plot(t, v(j) * ones(size(t)),'m')
    end
    hold off
end
end

function w = membership(i, v, x, N)
if i >= N
    w = zeros(size(x));
elseif i < -N
    w = zeros(size(x));
else
    i_set = -N:N;
    idx = find(i_set==i);
    w = (x >= v(idx) & x <= v(idx+1));
end
end