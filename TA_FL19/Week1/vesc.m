function vesc()
num_periods = 2;
num_points = 1e4 * num_periods;
time = linspace(0, num_periods, num_points);

h11_start = 0;
h11_end = 0.5;
h11 = zeros(1, num_points);
h11((mod(time, 1) >= h11_start) & (mod(time, 1) < h11_end)) = 1;

h21_start = 0.5;
h21_end = 0.98;
h21 = zeros(1, num_points);
h21((mod(time, 1) >= h21_start) & (mod(time, 1) < h21_end)) = 1;

h31_start = 0.98;
h31_end = 1;
h31 = zeros(1, num_points);
h31((mod(time, 1) >= h31_start) & (mod(time, 1) < h31_end)) = 1;

figure()
subplot(3, 1, 1)
plot(time, h11)
ylim([-0.1, 1.1]);
ylabel('H_{11}(t)')
subplot(3, 1, 2)
plot(time, h21)
ylim([-0.1, 1.1]);
ylabel('H_{21}(t)')
subplot(3, 1, 3)
plot(time, h31)
ylim([-0.1, 1.1]);
ylabel('H_{31}(t)')
xlabel('t (periods)')

i10 = 5;
v1s = 12;
v2s = 12.5;
i1s = h11 * i10;
i2s = h21 * i10;
v10 = h11 * v1s + h21 * v2s;

figure()
subplot(3, 1, 1)
plot(time, i1s)
ylabel('I_{1s}(t) (A)')
subplot(3, 1, 2)
plot(time, i2s)
ylabel('I_{2s}(t) (A)')
subplot(3, 1, 3)
plot(time, v10)
ylabel('V_{1o}(t) (V)')
xlabel('t (periods)')

i1s_rms = sqrt(0.5) * i10;
i2s_rms = sqrt(0.48) * i10;
v1o_rms = sqrt(0.5 * v1s^2 + 0.48 * v2s^2);

fprintf('I_1s_rms = %f A\n', i1s_rms)
fprintf('I_2s_rms = %f A\n', i2s_rms)
fprintf('V_1o_rms = %f V\n', v1o_rms)
end
