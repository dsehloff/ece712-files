
function [t,y] = triang_wave(amp,t_rise,t_fall,offset,start,stop,step)
%step = 1e-6;
t = start:step:stop;

%t_rise = 10e-3;
%t_fall = 20e-3;
%amp = 100;
ratio = (t_rise+t_fall)/t_fall;
rise = (mod(t*amp/t_rise,amp)-amp/2).*(mod(t,t_rise+t_fall) < t_rise);
fall = (mod(t*(-amp)/t_fall+ratio*amp,ratio*amp)-amp/2).*(mod(t,t_rise+t_fall) >= t_rise);
rise(find(abs(mod(t,t_rise+t_fall)-t_rise)<step/2))=amp/2;
rise(find(abs(mod(t,t_rise+t_fall))<step/2))=0;

fall(find(abs(mod(t,t_rise+t_fall))<step/2))=-amp/2;
fall(find(abs(mod(t,t_rise+t_fall)-t_rise)<step/2))=0;

triang = rise + fall;

y = triang + offset;
end
