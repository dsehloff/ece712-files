function matrix_sixstep2()
plot_switch_functions = true;
v_in_rms = 1/sqrt(2);
i_out_rms = 1/sqrt(2);
v_in_pk = v_in_rms * sqrt(2);
i_out_pk = i_out_rms * sqrt(2);



% displacement angles
phi_in = 0;
phi_out = 0;

alpha_in0 = 0;
alpha_out0 = 0;

% input frequency (Hz)
f_in = 180;
% output frequency (Hz)
f_out = 50;
% Number of periods to calculate (of lower frequency)
N_periods = 2;
T_max = 1 / min(f_in, f_out);

% Switching frequency
fs = 1e4;
Ts = 1/fs;

% Create Triangular wave
Tsample = Ts*1e-3;
t_rise = Ts/2;
t_fall = Ts/2;
amp = 1;
offset = 0.5;
start = 0;
stop = T_max * N_periods;
[t,triang] = triang_wave(amp,t_rise,t_fall,offset,start,stop, Tsample);

% Modulation Functions
% alpha is the phase angle of the voltage source
alpha_out = f_out * 2 * pi * t + alpha_out0;

% beta is the phase angle of the current source
beta_in0 = phi_in - alpha_in0;
beta_in = f_in * 2 * pi * t + beta_in0;

ma = cos(beta_in);
mb = cos(beta_in-2*pi/3);
mc = cos(beta_in+2*pi/3);

mu = cos(alpha_out);
mv = cos(alpha_out-2*pi/3);
mw = cos(alpha_out+2*pi/3);

% six step modulation
hap = ma>mb & ma>mc;
hbp = mb>ma & mb>mc;
hcp = mc>ma & mc>mb;
han = ma<mb & ma<mc;
hbn = mb<ma & mb<mc;
hcn = mc<ma & mc<mb;

% Source Waveforms
% alpha is the phase angle of the voltage source
alpha_in = f_in * 2 * pi * t + alpha_in0;
beta_out0 = phi_out - alpha_out0;
beta_out = f_out * 2 * pi * t + beta_out0;

va = v_in_pk*cos(alpha_in);
vb = v_in_pk*cos(alpha_in-2*pi/3);
vc = v_in_pk*cos(alpha_in+2*pi/3);

iu = i_out_pk*cos(beta_out);
iv = i_out_pk*cos(beta_out-2*pi/3);
iw = i_out_pk*cos(beta_out+2*pi/3);


% Synthesized Waveforms
vp = hap.*va + hbp.*vb + hcp.*vc;
vn = han.*va + hbn.*vb + hcn.*vc;
vpn = vp - vn;

m0=1;
vu_ref = m0*cos(alpha_out);
vv_ref = m0*cos(alpha_out-2*pi/3);
vw_ref = m0*cos(alpha_out+2*pi/3);

mup = ((vu_ref./vpn) + 1)/2;
mvp = ((vv_ref./vpn) + 1)/2;
mwp = ((vw_ref./vpn) + 1)/2;

mun = (-(vu_ref./vpn) + 1)/2;
mvn = (-(vv_ref./vpn) + 1)/2;
mwn = (-(vw_ref./vpn) + 1)/2;

hup = mup > triang;
hvp = mvp > triang;
hwp = mwp > triang;
hun = 1-hup;
hvn = 1-hvp;
hwn = 1-hwp;

vu = hup.*vp + hun.*vn;
vv = hvp.*vp + hvn.*vn;
vw = hwp.*vp + hwn.*vn;

vN = (vu + vv + vw)/3;
vuN = vu - vN;
vvN = vv - vN;
vwN = vw - vN;

vuv = vu - vv;
vvw = vv - vw;
vwu = vw - vu;

vu_avg = mup.*vp + mun.*vn;
vv_avg = mvp.*vp + mvn.*vn;
vw_avg = mwp.*vp + mwn.*vn;
vN_avg = (vu_avg + vv_avg + vw_avg)/3;
vuN_avg = vu_avg - vN_avg;
vvN_avg = vv_avg - vN_avg;
vwN_avg = vw_avg - vN_avg;


vuv_avg = vu_avg - vv_avg;
vvw_avg = vv_avg - vw_avg;
vwu_avg = vw_avg - vu_avg;

% CMC switching functions
hau = hup.*hap+hun.*han;
hbu = hup.*hbp+hun.*hbn;
hcu = hup.*hcp+hun.*hcn;
hav = hvp.*hap+hvn.*han;
hbv = hvp.*hbp+hvn.*hbn;
hcv = hvp.*hcp+hvn.*hcn;
haw = hwp.*hap+hwn.*han;
hbw = hwp.*hbp+hwn.*hbn;
hcw = hwp.*hcp+hwn.*hcn;

% Input currents
ia = hau.*iu + hav.*iv + haw.*iw;
ib = hbu.*iu + hbv.*iv + hbw.*iw;
ic = hcu.*iu + hcv.*iv + hcw.*iw;

% DC link currents
ip = hup.*iu + hvp.*iv + hwp.*iw;
in = hun.*iu + hvn.*iv + hwn.*iw;

% Throw currents
iau = hau.*iu;
ibu = hbu.*iu;
icu = hcu.*iu;

iav = hav.*iv;
ibv = hbv.*iv;
icv = hcv.*iv;

iaw = haw.*iw;
ibw = hbw.*iw;
icw = hcw.*iw;

% Throw Voltages
vau = va - vu;
vbu = vb - vu;
vcu = vc - vu;

vav = va - vv;
vbv = vb - vv;
vcv = vc - vv;

vaw = va - vw;
vbw = vb - vw;
vcw = vc - vw;

% filtered output voltage
windowLength = 0.99/fs; % length of window in seconds (choosing a value that gives clean average over switching period)
windowSize = round(1/Tsample * windowLength);
b = (1/windowSize)*ones(1,windowSize);
a = 1;

vuv_filt = filter(b,a,vuv);
vuN_filt = filter(b,a,vuN);


% Plots %
%%%%%%%%%


% From source to positive dc terminal
close all

if plot_switch_functions
    figure
    subplot(4,1,1)
    plot(t,ma,t,mb,t,mc)
    ylabel('m')
    legend('m_{a}','m_{b}','m_{c}')
    grid on
    subplot(4,1,2)
    plot(t,hap)
    ylabel('h_{ap}')
    axis([-inf inf -0.2 1.2])
    grid on
    subplot(4,1,3)
    plot(t,hbp)
    ylabel('h_{bp}')
    axis([-inf inf -0.2 1.2])
    grid on
    subplot(4,1,4)
    plot(t,hcp)
    xlabel('time (s)')
    ylabel('h_{cp}')
    axis([-inf inf -0.2 1.2])
    grid on
    
    % From source to negative dc terminal
    figure
    subplot(4,1,1)
    plot(t,ma,t,mb,t,mc)
    ylabel('m')
    legend('m_{a}','m_{b}','m_{c}')
    grid on
    subplot(4,1,2)
    plot(t,han)
    ylabel('h_{an}')
    axis([-inf inf -0.2 1.2])
    grid on
    subplot(4,1,3)
    plot(t,hbn)
    ylabel('h_{bn}')
    axis([-inf inf -0.2 1.2])
    grid on
    subplot(4,1,4)
    plot(t,hcn)
    ylabel('h_{cn}')
    axis([-inf inf -0.2 1.2])
    xlabel('time (s)')
    grid on
    
    % From positive dc terminal to the output
    figure
    subplot(4,1,1)
    plot(t, triang, 'color', [0,0,0]+0.95)
    hold on
    plot(t,mup,t,mvp,t,mwp)
    hold off
    ylabel('m')
    legend('carrier','m_{up}','m_{vp}','m_{wp}')
    grid on
    subplot(4,1,2)
    plot(t,hup)
    ylabel('h_{up}')
    axis([-inf inf -0.2 1.2])
    grid on
    subplot(4,1,3)
    plot(t,hvp)
    ylabel('h_{vp}')
    axis([-inf inf -0.2 1.2])
    grid on
    subplot(4,1,4)
    plot(t,hwp)
    ylabel('h_{wp}')
    axis([-inf inf -0.2 1.2])
    xlabel('time (s)')
    grid on
    
    % From negative dc terminal to the output
    figure
    subplot(4,1,1)
    plot(t, triang, 'color', [0,0,0]+0.95)
    hold on
    plot(t,mun,t,mvn,t,mwn)
    hold off
    ylabel('m')
    legend('carrier','m_{un}','m_{vn}','m_{wn}')
    grid on
    subplot(4,1,2)
    plot(t,hun)
    ylabel('h_{un}')
    axis([-inf inf -0.2 1.2])
    grid on
    subplot(4,1,3)
    plot(t,hvn)
    ylabel('h_{vn}')
    axis([-inf inf -0.2 1.2])
    grid on
    subplot(4,1,4)
    plot(t,hwn)
    ylabel('h_{wn}')
    axis([-inf inf -0.2 1.2])
    xlabel('time (s)')
    grid on
    
end

% Voltage and Current Waveforms
figure
plot(t, vpn)
title('dc bus voltage')
xlabel('time (s)')
grid on

figure
plot(t, mup, t, mvp, t, mwp)
title('VSB Modulating Functions')
xlabel('time (s)')
grid on

figure
subplot(2,1,1)
plot(t, vuN_avg, t, vvN_avg, t, vwN_avg)
title('Average output ln voltage')
subplot(2,1,2)
plot(t, vuv_avg, t, vvw_avg, t, vwu_avg)
title('Average output ll voltage')
xlabel('time (s)')
grid on

figure
subplot(2,1,1)
plot(t, vuN, t, vvN, t, vwN)
title('Output ln voltage')
subplot(2,1,2)
plot(t, vuv, t, vvw, t, vwu)
title('Output ll voltage')
xlabel('time (s)')
grid on


figure
subplot(2,1,1)
plot(t, vuN)
ylabel('v_{uN}')
title('Output voltage v_{uN} and first order filtered v_{uN}')
subplot(2,1,2)
plot(t, vuN_filt)
ylabel('v_{uN,filtered}')
xlabel('time (s)')
grid on

figure
subplot(2,1,1)
plot(t, vuv)
ylabel('v_{uv}')
title('Output voltage v_{uv} and first order filtered v_{uv}')
subplot(2,1,2)
plot(t, vuv_filt)
ylabel('v_{uv,filtered}')
xlabel('time (s)')
grid on

figure
subplot(2,1,1)
plot(t, ip)
ylabel('i_{p}')
title('DC Link Currents')
subplot(2,1,2)
plot(t, in)
ylabel('i_{n}')
xlabel('time (s)')
grid on

figure
subplot(4,2,1)
plot(t,va,t,vb,t,vc)
title('input v')
grid on
subplot(4,2,3)
plot(t,ia,t,ib,t,ic)
title('input i')
grid on
subplot(4,2,5)
plot(t,vuN,t,vvN,t,vwN)
title('output v')
grid on
subplot(4,2,7)
plot(t,iu,t,iv,t,iw)
title('output i')
xlabel('time (s)')
grid on
subplot(4,2,2)
plot(t,va)
title('phase a input v')
grid on
subplot(4,2,4)
plot(t,ia)
title('phase a input i')
grid on
subplot(4,2,6)
plot(t,vuN)
title('phase u output line-neutral v')
grid on
subplot(4,2,8)
plot(t,iu)
title('phase u output i')
xlabel('time (s)')
grid on

% Throw Voltage Waveforms
figure
subplot(9,1,1)
plot(t,vau)
title('Throw voltages')
ylabel('v_{au}')
grid on
subplot(9,1,2)
plot(t,vbu)
ylabel('v_{bu}')
grid on
subplot(9,1,3)
plot(t,vcu)
ylabel('v_{cu}')
grid on
subplot(9,1,4)
plot(t,vav)
ylabel('v_{av}')
grid on
subplot(9,1,5)
plot(t,vbv)
ylabel('v_{bv}')
grid on
subplot(9,1,6)
plot(t,vcv)
ylabel('v_{cv}')
grid on
subplot(9,1,7)
plot(t,vaw)
ylabel('v_{aw}')
grid on
subplot(9,1,8)
plot(t,vbw)
ylabel('v_{bw}')
grid on
subplot(9,1,9)
plot(t,vcw)
ylabel('v_{cw}')
grid on

% Throw Current Waveforms
figure
subplot(3,1,1)
plot(t,iau,t,ibu,t,icu)
title('Throw currents')
ylabel('i_{au,bu,cu}')
legend('i_{au}','i_{bu}','i_{cu}')
grid on
subplot(3,1,2)
plot(t,iav,t,ibv,t,icv)
title('Throw currents')
ylabel('i_{av,bv,cv}')
legend('i_{av}','i_{bv}','i_{cv}')
grid on
subplot(3,1,3)
plot(t,iaw,t,ibw,t,icw)
title('Throw currents')
ylabel('i_{aw,bw,cw}')
legend('i_{aw}','i_{bw}','i_{cw}')
grid on
end
